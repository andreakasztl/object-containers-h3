package ex1;

public class Persoane {
    /*
    You need to store Persoane in a TreeSet. Define 2 Comparators (one for name - nume and one for age-varsta) that will be used when creating the TreeSet;

Add Persoane to the TreeSet; iterate throug the treeset and print the name and the age;
NOTE: if you are using a Comparator, the class Persoana should not implement Comparable anymore.
     */

    private String nume;
    private int varsta;

    public Persoane(String nume, int varsta) {
        this.nume = nume;
        this.varsta = varsta;
    }

    public Persoane(String nume) {
        this.nume = nume;
        this.varsta = 18;
    }

    public String getNume() {
        return nume;
    }

    public int getVarsta() {
        return varsta;
    }

    @Override
    public String toString() {
        return "Persoane{" +
                "nume='" + nume + '\'' +
                ", varsta=" + varsta +
                '}';
    }
}
