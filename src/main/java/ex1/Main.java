package ex1;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        Persoane p1 = new Persoane("Dana", 14);
        Persoane p2 = new Persoane("David", 28);
        Persoane p3 = new Persoane("Diana", 34);
        Persoane p4 = new Persoane("Denisa", 5);
        Persoane p5 = new Persoane("Denis", 45);


        Comparator<Persoane> numeComparator = Comparator.comparing(Persoane::getNume);

        Comparator<Persoane> varstaComparator = Comparator.comparingInt(Persoane::getVarsta);

        Set<Persoane> persoaneDupaNume = new TreeSet<>(numeComparator);

        persoaneDupaNume.add(p1);
        persoaneDupaNume.add(p2);
        persoaneDupaNume.add(p3);
        persoaneDupaNume.add(p4);
        persoaneDupaNume.add(p5);

        System.out.println("Persoane Treeset, Comparator dupa nume:");

        for (Persoane p : persoaneDupaNume){
            System.out.println(p);
        }

        Set<Persoane> persoaneDupaVarsta = new TreeSet<>(varstaComparator);
        persoaneDupaVarsta.addAll(persoaneDupaNume);

        System.out.println();
        System.out.println("Persoane Treeset, Comparator dupa varsta:");


        for (Persoane p : persoaneDupaVarsta){
            System.out.println(p);
        }



    }
}
