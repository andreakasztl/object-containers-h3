package ex2;


import java.util.Objects;

public class Persoana{
    private String nume;
    private int varsta;

    @Override
    public String toString() {
        return "Persoana{" +
                "nume='" + nume + '\'' +
                ", varsta=" + varsta +
                '}';
    }

    public Persoana(String nume) {
        this.nume = nume;
        varsta = 40;
    }

    public String getNume() {
        return nume;
    }

    public int getVarsta() {
        return varsta;
    }

    public Persoana(String nume, int varsta) {
        this.nume = nume;
        this.varsta = varsta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Persoana)) return false;
        Persoana persoana = (Persoana) o;
        return varsta == persoana.varsta &&
                nume.equals(persoana.getNume());
    }

    @Override
    public int hashCode() {
        return Objects.hash(nume, varsta);
    }


    /*    public int compareTo(Object o) {
        Persoana p = (Persoana) o;
        return this.getNume().compareTo(p.nume);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Persoana)) return false;
        Persoana persoana = (Persoana) o;
        return varsta == persoana.varsta &&
                Objects.equals(nume, persoana.nume);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nume, varsta);
    }*/
}
