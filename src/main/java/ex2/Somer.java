package ex2;

public class Somer extends Persoana {
    public Somer(String nume) {
        super(nume);
    }

    public Somer(String nume, int varsta) {
        super(nume, varsta);
    }

    @Override
    public String toString() {
        return "Somer{" +
                "nume='" + getNume() + '\'' +
                ", varsta=" + getVarsta() +
                '}';
    }

}
