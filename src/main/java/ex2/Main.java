package ex2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        //create Persoana
        Student p1 = new Student("Dana", 14);
        Persoana p3 = new Angajat("Diana", 34);
        Persoana p5 = new Angajat("Denis", 45);
        //same as p3
        Angajat p6 = new Angajat("Diana", 34);

        //create addresses
        Adresa adresaParc = new Adresa("Alexandru Vaida Voevod",71,"Cluj-Napoca","Cluj","Romania");
        Adresa adresaDans = new Adresa("Strada Regele Ferdinand", 22, "Cluj-Napoca", "Cluj", "Romania");
        Adresa adresaStadion = new Adresa("C. d'Arístides Maillol",12,"Barcelona", "Barcelona","Spain");
        Adresa adresaStadionMilano = new Adresa("Piazzale Angelo Moratti", 0, "Milano","MI", "Italy");
        Adresa addressBalet = new Adresa("Bol'shoy Prospekt Vasil'yevskogo Ostrova", 83, "St Petersburg","", "Russia");
        Adresa addressbalet2 = new Adresa("Toom-Kooli",11,"Tallin","", "Estonia");


        //create hobbies
        List<Adresa> footballAddresses = new ArrayList<>();
        footballAddresses.add(adresaParc);
        footballAddresses.add(adresaStadion);
        footballAddresses.add(adresaStadionMilano);
        Hobby football = new Hobby("football",5,footballAddresses);

        List<Adresa> danceAddresses = new ArrayList<>();
        danceAddresses.add(adresaDans);
        danceAddresses.add(addressBalet);
        danceAddresses.add(addressbalet2);
        Hobby dance = new Hobby("dance",7,danceAddresses);

        List<Adresa> balletAddresses = new ArrayList<>();
        balletAddresses.add(addressBalet);
        balletAddresses.add(addressbalet2);
        Hobby ballet = new Hobby("ballet",14, balletAddresses);

        Hobby singing = new Hobby("singing",3);


        Map<Persoana, List<Hobby>> persoaneHobby = new HashMap<>();

        List<Hobby> p1Hobbies = new ArrayList<>();
        p1Hobbies.add(dance);
        p1Hobbies.add(ballet);
        persoaneHobby.put(p1,p1Hobbies);

        List<Hobby> p3Hobbies = new ArrayList<>();
        p3Hobbies.add(ballet);
        persoaneHobby.put(p3,p3Hobbies);

        List<Hobby> p5Hobbies = new ArrayList<>();
        p5Hobbies.add(football);
        persoaneHobby.put(p5,p5Hobbies);

        List<Hobby> p6Hobbies = new ArrayList<>();
        p6Hobbies.add(singing);
        //overwrites p3
        persoaneHobby.put(p6,p6Hobbies);

        for (Persoana p: persoaneHobby.keySet()){
            System.out.println(p.getNume() + ":" + "\n");
            System.out.println(persoaneHobby.get(p));
            System.out.println();
        }
















    }
}
