package ex2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Hobby {
    private String numeHobby;
    private int frecventa;
    private List<Adresa> adrese;

    public Hobby(String numeHobby, int frecventa) {
        this.numeHobby = numeHobby;
        this.frecventa = frecventa;
        adrese = new ArrayList<>();
    }

    public Hobby(String hobbyName, int frecventa, List<Adresa> adrese) {
        this.numeHobby = hobbyName;
        this.frecventa = frecventa;
        this.adrese = adrese;
    }

    public void addAddress(Adresa address){
        adrese.add(address);
    }

    public String getNumeHobby() {
        return numeHobby;
    }

    public int getFrecventa() {
        return frecventa;
    }

    public List<Adresa> getAdrese() {
        List<Adresa> addressesCopy = new ArrayList<>();
        addressesCopy.addAll(adrese);
        return addressesCopy;
    }

    private static String getCountries(List<Adresa> addresses){
        StringBuilder s = new StringBuilder();

        Set<String> countries = new HashSet<>();

        for (Adresa a : addresses){
            countries.add(a.getTara());
        }
        for (String c : countries){
            s.append(c + ", ");
        }
        if (s.length() > 0)
            s.delete(s.length()-2,s.length());
        else s.append("none");
        return s.toString();
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "numeHobby = '" + numeHobby + '\'' +
                ", countries = " + getCountries(adrese) +
                '}';
    }
}
