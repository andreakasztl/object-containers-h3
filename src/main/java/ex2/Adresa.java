package ex2;

public class Adresa {
    private String strada;
    private int nr;
    private String oras;
    private String judet;
    private String tara;


    public Adresa(String strada, int nr, String oras, String judet, String tara) {
        this.strada = strada;
        this.nr = nr;
        this.oras = oras;
        this.judet = judet;
        this.tara = tara;
    }

    public String getTara() {
        return tara;
    }
}